# Soil study api

A Django 3.2 API application

## Steps to run the API.
#
### Prerequisites 
#
1. Make sure you has installed docker and docker-compose in your operative system.

2. It's no mandatory but it's recommendable use vscode as a code editor.

3. If you use vscode install the extension Remote - Containers (ms-vscode-remote.remote-containers)



### Steps
#
#### 1. Create an instance of the conteiners, Postgres database, Django API and PGadmin.
#
* If you doesn't want to use vscode with Remote - Containers extension 

  > Remember that the use of this extension allow vscode to read all libraries from the container.

  Run in the root project folder:
  ```sh
  docker-compose up
  ```
* If you will  use vscode with Remote - Containers extension 

  1. Open project folder in vscode.
  2. Follow the steps in this tutorial https://code.visualstudio.com/docs/remote/containers#_quick-start-open-an-existing-folder-in-a-container. (Keep in mind that you will use a existing Dockerfile)



#### 2. Open a container terminal.
#
* If use vscode with Remote - Containers extension only open a new terminar in the interface.

* Else run `docker ps` in a terminal and you will see a list of all active containers, copy the `conteiner_id` of Django container and run:

  ```sh
  docker exec -it <container_id> /bin/sh
  ```


#### 3. Run API.
#
Run in the conteiner terminal:
```sh 
python manage.py runserver 0.0.0.0:8000
```
## Additional Info
#
* If you want to add python libraries add it in  requirements.txt and rebuild the images usign the command `docker-compose up --build` 

* In some cases the files created inside the docker image using vscode are created as root to change the permissions, in container terminal run:

  ```sh
  chmod 766 -R .
  chmod 755 -R ./data
  ```

## Util link references
#
* https://docs.docker.com/compose/
* https://www.djangoproject.com/start/overview/
* https://docs.djangoproject.com/en/3.2/ref/django-admin/
* https://docs.djangoproject.com/en/3.2/topics/migrations/
* https://docs.djangoproject.com/en/3.2/topics/auth/default/


