from django.db import models



#  MODEL BASE DE DATOS QGIS 
class Geologia(models.Model):
    idgeologia = models.AutoField(primary_key=True)
    objgeografico = models.ForeignKey("geologia", verbose_name=("idgeologia"), on_delete=models.CASCADE)
    namegeografico = models.CharField(max_length=30)
    consistencia = models.CharField(max_length=40)
    nomeclatura = models.CharField(max_length=40)
    cataloobj = models.CharField(max_length=50)
    caracteristica = models.CharField(max_length=100)
    shapearea = models.CharField(max_length=30)
    shapelengts = models.CharField(max_length=30)
    edad = models.CharField(max_length=30)
    comentarios = models.CharField(max_length=30)