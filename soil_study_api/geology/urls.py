from django.urls import path

from . import views

urlpatterns = [
    path('geologia', views.GeologiaCRUD)
]