from django.http import JsonResponse, HttpResponse
import json
#from rest_framework.authentication import BasicAuthentication
#from soil_study_api.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict
from soil_study_api.geology.models import Geologia
from django.core import serializers


@api_view(['POST','GET','PATCH','DELETE'])
#@authentication_classes([BasicAuthentication])
#@permission_classes([IsAuthenticated])
def GeologiaCRUD(request, id):
    print(request.body)
    if request.method == 'POST':
        data = json.loads(request.body)
        print(data)
        newPerforation = Geologia(**data)
        newPerforation.save()
        perforation = model_to_dict(newPerforation)
        print(perforation)
        return JsonResponse(perforation)
    if request.method == 'GET':
        perforationRegisters = serializers.serialize('json',Geologia.objects.all())
        return HttpResponse(perforationRegisters, content_type="text/json-comment-filtered")
    if request.method == 'DELETE':
        Geologia.objects.get(id = id) 
        datos= Geologia.delete(id)
        operacion = datos
        data = {}
        if operacion:
            data["success"] = "delete successfull"
        else:
            data["failure"] = "delete failed"
        return JsonResponse(data)
