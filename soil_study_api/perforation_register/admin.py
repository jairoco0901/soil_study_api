from typing import OrderedDict
from django.contrib import admin
from . import models


class registerPerforarionmodel(admin.ModelAdmin):
    list_display = ('idSondeos', 'idMuestra', 'profundidadInicial', 'profundidadFinal', 'spt6','spt12','spt18','tipoMuestra', 'clasificacionesPrimarias')
    ordering = ('idSondeos', )

admin.site.register(models.Sondeo),
admin.site.register(models.registroDePerforacion, registerPerforarionmodel )
