from django.urls import path

from . import views

urlpatterns = [
    ############## SONDEOS   #####################
    path('sondeos-post',views.sondeos_post_CRUD),
    path('sondeos-get',views.sondeos_get_CRUD),
    path('sondeos-delete-all',views.delete_all_sondeos_CRUD),
    path('sondeos-delete',views.sondeo_delete_CRUD),
    ############## Registro De Perforacion ###############
    path('RegistroDePerforacion-post',views.RegistroDePerforacion_post_CRUD),
    path('RegistroDePerforacion-get',views.RegistroDePerforacion_get_CRUD),
    path('RegistroDePerforacion-delete-all',views.delete_all_RegistroDePerforacion_CRUD),
    path('RegistroDePerforacion-delete',views.RegistroDePerforacion_delete_CRUD),
    
       

]