from os import PRIO_PGRP
from textwrap import indent
from django.http import JsonResponse, HttpResponse, response
from django.shortcuts import render
import json
from rest_framework import status
#from rest_framework.authentication import BasicAuthentication
#from soil_study_api.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict

from soil_study_api.perforation_register.models import registroDePerforacion,Sondeo
from django.core import serializers
import pandas as pd
import numpy as np
from math import sqrt


@api_view([ 'POST'])
def sondeos_post_CRUD(request, id=0):
    if request.method == "PUT":
        if id==0:
            updsondeos = Sondeo.objects.get(pk=id)
            form = serialize(instance=updsondeos)
            return JsonResponse(form)
    if request.method == 'POST':
        datos =json.loads(request.body)
        newSondeos = Sondeo(**datos)
        newSondeos.save()
        sondeo= model_to_dict(newSondeos)
        return JsonResponse(sondeo)

@api_view(['GET'])
def sondeos_get_CRUD(request):
    if request.method == 'GET':
        sondeos = pd.DataFrame(Sondeo.objects.all().values())
        return JsonResponse(sondeos.to_dict('records'), safe=False)

@api_view(['DELETE'])
def delete_all_sondeos_CRUD(request):
    #metodo para borrar todos los registros de sondeos
    if request.method == 'DELETE':
        count = Sondeo.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def sondeo_delete_CRUD(request,id):
    if request.method == 'DELETE':
        sondeo = Sondeo.objects.get(pk=id)
        sondeo.delete()
        return HttpResponse(sondeo, content_type="text/json-comment-filtered")


############################################################################################


@api_view([ 'POST'])
def RegistroDePerforacion_post_CRUD(request, id=0):
    if request.method == "PUT":
        if id==0:
            upddato = registroDePerforacion.objects.get(pk=id)
            form = serialize(instance=upddato)
            return JsonResponse(form)
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = registroDePerforacion(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)

@api_view(['GET'])
def RegistroDePerforacion_get_CRUD(request):
    if request.method == 'GET':
        perforation_register = pd.DataFrame(registroDePerforacion.objects.all().values())
        return JsonResponse(perforation_register.to_dict('records'), safe=False)
        


@api_view(['DELETE'])
def delete_all_RegistroDePerforacion_CRUD(request):
    #metodo para borrar todos los registros de sondeos
    if request.method == 'DELETE':
        count = registroDePerforacion.objects.all().delete()
        return JsonResponse({'message': '{}were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def RegistroDePerforacion_delete_CRUD(request,id):
    if request.method == 'DELETE':
        perforacion = registroDePerforacion.objects.get(pk=id)
        perforacion.delete()
        return HttpResponse(perforacion, content_type="text/json-comment-filtered")