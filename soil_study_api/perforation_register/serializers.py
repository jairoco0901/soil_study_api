from rest_framework import serializers

from models import  registroDePerforacion, sondeo
from soil_study_api.perforation_register import models


class perforationRegisterCRUD(serializers.ModelSerializer):
    class Meta:
        model = registroDePerforacion
        fields = '__all__'






class serializersForm(models.sondeo):

    class Meta:
        model = sondeo
        fields = ('coordenadas','horaInicial','horaFinal','absisa','idSondeos')
        labels = {
            'idSondeos':'idSondeos',
            'emp_code':'EMP. Code'
        }

    def __init__(self, *args, **kwargs):
        super(serializersForm,self).__init__(*args, **kwargs)
        self.fields['position'].empty_label = "Select"
        self.fields['emp_code'].required = False