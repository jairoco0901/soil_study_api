from django.contrib import admin
from . import models



class infoAdmin(admin.ModelAdmin):
    list_display = ('agencia','contratante','contratista','coordenadas', 'ciudad', 'barrio', 'direccion')


admin.site.register(models.informacionGeneral, infoAdmin)