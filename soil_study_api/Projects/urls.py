from django.urls import path
from django.conf.urls import url 

from . import views

urlpatterns = [
    
    path('project_post', views. add_project_post),
    path('project_get_CRUD', views.project_get_CRUD),
    path('delete_all_project_CRUD', views.delete_all_project_CRUD),
     
    #url(r'^api/tutorials$', views.tutorial_list),
    #url(r'^api/tutorials/published$', views.tutorial_list_published)
]
