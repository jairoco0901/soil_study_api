from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, make_password
from django.db.models.base import Model



class informacionGeneral(models.Model):
    agencia     = models.CharField(max_length=60,  blank=False, default='')
    contratista = models.CharField(max_length=60)
    contratante = models.CharField(max_length=60)
    coordenadas = models.CharField(max_length=60)
    ciudad      = models.CharField(max_length=60)
    barrio      = models.CharField(max_length=60)
    direccion   = models.CharField(max_length=60)
    descripcionProyecto = models.CharField(max_length=300)