from rest_framework import serializers
from soil_study_api.Projects.models import informacionGeneral

class infoProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = informacionGeneral
        fields = ('agencia','contratante','contratista','coordenadas', 'ciudad', 'barrio', 'direccion')
