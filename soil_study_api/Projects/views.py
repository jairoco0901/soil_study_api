from django.http import JsonResponse, HttpResponse, response
import json
from rest_framework.parsers import JSONParser
#from rest_framework.authentication import BasicAuthentication
#from soil_study_api.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token

from django.forms.models import model_to_dict
from soil_study_api.Projects.models import  informacionGeneral
from soil_study_api.Projects.serializers import infoProjectSerializer

from rest_framework import status
import pandas as pd

# @api_view(['POST', 'GET', 'DELETE'])
# def add_project(request):
#     if request.method == 'POST':
#         datos =json.loads(request.body)
#         newproject = informacionGeneral(**datos)
#         newproject.save()
#         dato= model_to_dict(newproject)
#         return JsonResponse(dato, status=status.HTTP_201_CREATED)
#     elif request.method ==  'GET':
#         info = informacionGeneral.objects.all()
#         serializer = infoProjectSerializer(info, many=True)
#         return  JsonResponse(serializer.data, safe = False)
#     elif request.method == 'DELETE':
#         count = informacionGeneral.objects.all().delete()
#     return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view([ 'POST'])
def  add_project_post(request, id=0):
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = informacionGeneral(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)


@api_view(['GET'])
def project_get_CRUD(request):
    if request.method == 'GET':
        dato = pd.DataFrame(informacionGeneral.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)


@api_view(['DELETE'])
def delete_all_project_CRUD(request):
    #metodo para borrar todos los registros de sondeos
    if request.method == 'DELETE':
        count = informacionGeneral.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



# api_view(['GET', 'POST', 'DELETE'])
# def tutorial_list(request):
#     if request.method == 'GET':
#         projects = informacionGeneral.objects.all()
#         # agencia = request.query_params.get('agencia', None)
#         # if agencia is not None:
#         #     projects = projects.filter(title__icontains=agencia)
        
#         serializer = infoProjectSerializer(projects, many=True)
#         return JsonResponse(serializer.data, safe=False)
#         # 'safe=False' for objects serialization

#     elif request.method == 'POST':
#         projects_data = json.loads(request.body)
#         serializer = infoProjectSerializer(projects_data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=status.HTTP_201_CREATED) 
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET','PUT','DELETE'])
# def info_project_details(request, pk):
#     try:
#         projecto = informacionGeneral.objects.get(pk=pk)
#     except informacionGeneral.DoesNotExist:
#         return JsonResponse({'message': 'The tutorial does not exist'}, status=status.HTTP_404_NOT_FOUND)
    
#     if request.method == 'GET':
#         serializer = infoProjectSerializer(projecto)
#         return JsonResponse(serializer.data) 

#     elif request.method == 'PUT':
#         serializer= infoProjectSerializer(projecto, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#     elif request.method == 'DELETE':
#         projecto.delete()
#         return JsonResponse({'message': 'Tutorial was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


# @api_view(['GET'])
# def tutorial_list_published(request):
#     projecto = informacionGeneral.objects.filter(published=True)
#     if request.method == 'GET': 
#         serializer = infoProjectSerializer(projecto, many=True)
#         return JsonResponse(serializer.data, safe=False)
