from django.http import JsonResponse, HttpResponse, response
import json
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from soil_study_api.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict
from soil_study_api.geo_images.models import  imgGoogleMaps, imgAtlasGeologico, imgOpenStret
from soil_study_api.geo_images.serializers import atlasSerializer, googleSerializer, openstreetSerializer
from django.core import serializers



@api_view(['GET', 'POST'])
def imagenGoogle(request):

    #get all articles
    if request.method == 'GET':
        articles = imgGoogleMaps.objects.all()
        serializer = googleSerializer(articles, many=True)
        return JsonResponse(serializer.data)

    elif request.method == 'POST':
        serializer = googleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response(serializer.data, status=status.HTTP_201_CREATED)
        return response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def imagenAtlas(request):
    #get all articles
    if request.method == 'GET':
        articles = imgAtlasGeologico.objects.all()
        serializer = atlasSerializer(articles, many=True)
        return JsonResponse(serializer.data)

    elif request.method == 'POST':
        serializer = atlasSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response(serializer.data, status=status.HTTP_201_CREATED)
        return response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'POST'])
def imagenOpenStreet(request):
    #get all articles
    if request.method == 'GET':
        articles = imgOpenStret.objects.all()
        serializer = openstreetSerializer(articles, many=True)
        return JsonResponse(serializer.data)

    elif request.method == 'POST':
        serializer = openstreetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response(serializer.data, status=status.HTTP_201_CREATED)
        return response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT', 'DELETE'])
def google_details(request, pk):
    try:
        article = imgGoogleMaps.objects.get(pk=pk)
    except imgGoogleMaps.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = googleSerializer(article)
        return response(serializer.data)

    elif request.method == 'PUT':
        serializer = googleSerializer(article, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response(serializer.data)
        return response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        article.delete()
        return response(status=status.HTTP_204_NO_CONTENT)