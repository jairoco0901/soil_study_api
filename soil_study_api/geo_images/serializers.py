from rest_framework import serializers

from soil_study_api.geo_images.models import imgGoogleMaps, imgAtlasGeologico, imgOpenStret

class googleSerializer(serializers.ModelSerializer):
    class Meta:
        model = imgGoogleMaps
        fields = ['name',]

class atlasSerializer(serializers.ModelSerializer):
    class Meta:
        model = imgAtlasGeologico
        fields = ['name',]


class openstreetSerializer(serializers.ModelSerializer):
    class Meta:
        model = imgOpenStret
        fields = ['name',]
