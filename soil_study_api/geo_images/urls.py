from django.urls import path

from . import views

urlpatterns = [
    #google
    path('imgGoogle', views.imagenGoogle),
    path('articles/<int:pk>/', views.google_details),


    #atlas
    path('imgAtlas', views.imagenAtlas),


    #openstreet
    path('imgOpen', views.imagenOpenStreet),


    #path('articles/', ArticleList.as_view()),
    #path('articles/<int:id>/', ArticleDetails.as_view())
    #path('articles/', article_list),

]