from django.db import models



class imgGoogleMaps(models.Model):
    googlemap = models.ImageField(upload_to='logo/%Y/%M/%d', null =True, blank=True)
    name = models.CharField(max_length=10, null=True, blank=True)

class imgOpenStret(models.Model):
    openstreet = models.ImageField(upload_to='logo/%Y/%M/%d', null =True, blank=True)
    name = models.CharField(max_length=10, null=True, blank=True)

class  imgAtlasGeologico(models.Model):
    atlasGeologico = models.ImageField(upload_to='logo/%Y/%M/%d', null =True, blank=True)
    name = models.CharField(max_length=10, null=True, blank=True)

