from django.contrib import admin
from . import models



class googleMapmodels(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(models.imgGoogleMaps, googleMapmodels)


class openStreetmodels(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(models.imgOpenStret, openStreetmodels)


class atlasGeomodels(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(models.imgAtlasGeologico, atlasGeomodels)