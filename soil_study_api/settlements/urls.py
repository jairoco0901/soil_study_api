from django.urls import path

from . import views

urlpatterns = [

    path('datosIniciales_CRUD', views.datosIniciales_CRUD),
    path('datosIniciales-delete', views.datosIniciales_delete_CRUD),
    
    path('datosPrimary_CRUD', views.datosPrimary_CRUD),
    path('datosPrimary-delete', views.datosPrimary_delete_CRUD),

    path('diferenciales_CRUD', views.diferenciales_CRUD),
    path('diferenciales-delete', views.diferenciales_delete_CRUD),

    path('servicios_CRUD', views.servicios_CRUD),
    path('servicios-delete/<str:id>', views.servicios_delete_CRUD),

    path('dimensionesPersonalizadas_CRUD', views.dimensionesPersonalizadas_CRUD),
    path('dimensionesPersonalizadas-delete', views.dimensionesPersonalizadas_delete_CRUD),

    path('preciosUnitarios_CRUD', views.preciosUnitarios_CRUD),
    path('preciosUnitarios-delete', views.preciosUnitarios_delete_CRUD),

    

 
    
    # path('Asentamiento', views.Asentamiento2),
    # path('AsentRegister', views.AsentRegister),   
    # path('dimensionMayor', views.dimensionMayor), 
    # path('Excentricidades', views.Excentricidades), 
    # path('AsentamientoDiferencial', views.AsenD),
    # path('dimensionesConstruccion', views.dimensionesConstruccion),
    # path('CantidadObra', views.cant_ObraPres),
    # path('Verificar', views.verificar),    
    # path('secuencia', views.secuencia),
    # path('presupuesto',views.presupuesto),
    # path('AsentamiendoPersonalizado',views.AsentamiendoPersonalizado),

]







