from django.db import models
from django.db.models.deletion import CASCADE



class servicios(models.Model):
    joints   =  models.CharField(primary_key=True, max_length=10, editable=True)
    fuerza1  = models.DecimalField(max_digits=10, decimal_places=3)
    fuerza2  = models.DecimalField(max_digits=10, decimal_places=3)
    fuerza3  = models.DecimalField(max_digits=10, decimal_places=3)
    momento1 = models.DecimalField(max_digits=10, decimal_places=3)
    momento2 = models.DecimalField(max_digits=10, decimal_places=3)
    momento3 = models.DecimalField(max_digits=10, decimal_places=3)
    def __str__(self):
            return self.joints
    


class diferenciales(models.Model):
    joints = models.IntegerField(verbose_name='JOINTS')
    vecinos = models.IntegerField()
    distancias = models.DecimalField(max_digits=10,decimal_places=3)
    criterioDiferencial=models.DecimalField(max_digits=10, decimal_places=4)


class dimensionesPersonalizadas(models.Model):
    joints= models.ForeignKey(servicios,null=False,blank=False, on_delete=models.CASCADE)
    bpersonalizado=models.DecimalField(max_digits=10,decimal_places=3)
    lpersonalizado=models.DecimalField(max_digits=10,decimal_places=3)


CHOICES2 = [
    ('si','SI'),
    ('no','NO'),
    
]


class preciosUnitarios(models.Model):
    precioExc=models.DecimalField(max_digits=10,decimal_places=3)
    precioConcZ=models.DecimalField(max_digits=10,decimal_places=3)
    precioConcP=models.DecimalField(max_digits=10,decimal_places=3)
    precioRelleno=models.DecimalField(max_digits=10,decimal_places=3)
    preciosolado=models.DecimalField(max_digits=10,decimal_places=3)
    precioAceroZapata=models.DecimalField(max_digits=10,decimal_places=3)
    precioAceroPedestal=models.DecimalField(max_digits=10,decimal_places=3)
    
CHOICES3 = [
    ('mayorBL','MayorBL'),
    ('menorBL','MayorBL'),
    
]

CHOICES1 = [
    ('hansen','Hansen'),
    ('vesic','Vesic'),
    
]



class datosIniciales(models.Model):
    nivelFeatico = models.DecimalField(max_digits=10, decimal_places=4)    
    profundidadCimentacion = models.DecimalField(max_digits=10, decimal_places=4)
    factorResistencia = models.DecimalField(max_digits=10, decimal_places=4)
    modelo=models.CharField(choices=CHOICES1,max_length=32)
    asentamimientoMaximo = models.DecimalField(max_digits=10,decimal_places=3)
    OCR=models.CharField(choices=CHOICES2,max_length=32)
    h=models.DecimalField(max_digits=10,decimal_places=3)
    aumentoB = models.DecimalField(max_digits=10, decimal_places=4)
    cifrasDecimales = models.IntegerField()
    salto=models.DecimalField(max_digits=10,decimal_places=3)
    personalizado=models.CharField(choices=CHOICES2,max_length=32)
    



class datosPrimary(models.Model):
    espesorZapata=models.DecimalField(max_digits=10,decimal_places=3)
    espesorSolado=models.DecimalField(max_digits=10,decimal_places=3)
    espesorPedestal=models.DecimalField(max_digits=10,decimal_places=3)
    condicion=models.CharField(choices=CHOICES3,max_length=32)
    cuantiaAceroZapata=models.DecimalField(max_digits=10,decimal_places=3)
    cuantiaAceroPedestal=models.DecimalField(max_digits=10,decimal_places=3)









    




