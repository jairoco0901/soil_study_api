from ast import Try
from wsgiref.util import request_uri
from django.db.models.expressions import F
from django.http import JsonResponse, HttpResponse
import json
from django.shortcuts import render
from numpy.core.fromnumeric import mean
from numpy.lib.shape_base import kron
from pandas.core.indexes.base import Index
from pandas.io.formats.format import return_docstring
#from rest_framework.authentication import BasicAuthentication
#from soil_study_api.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict
#from soil_study_api.portant_capacity.views import Cap
from soil_study_api.settlements.models import   datosPrimary, diferenciales,  servicios, preciosUnitarios, datosIniciales,dimensionesPersonalizadas
from soil_study_api.Geotechnical_profile.models import perfilGeotecnico


from rest_framework import status
from django.core import serializers
import pandas as pd
import numpy as np
import math
from rest_framework import status



###################################################


##############################################################################################

@api_view([ 'POST','PUT','GET','DELETE'])
def diferenciales_CRUD(request, id=0):    
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = diferenciales(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)
    if request.method == 'GET':
        dato = pd.DataFrame(diferenciales.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)
    if request.method == 'DELETE':
        count = diferenciales.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



@api_view(['DELETE'])
def diferenciales_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = diferenciales.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")

######################################################


@api_view([ 'POST','PUT','GET','DELETE'])
def servicios_CRUD(request, id=0):
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = servicios(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)
    if request.method == 'PUT':
        datos =json.loads(request.body)
        datosupdate=servicios(**datos)
        datosupdate.save()
        dato= model_to_dict(datosupdate)
        return JsonResponse(dato)
    if request.method == 'GET':
        dato = pd.DataFrame(servicios.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)
    
    if request.method == 'DELETE':
        count = servicios.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)
    

@api_view(['DELETE'])
def servicios_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = servicios.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")



#################################
#perso
######################################################



@api_view([ 'POST','PUT','GET','DELETE'])
def dimensionesPersonalizadas_CRUD(request, id=0):
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = dimensionesPersonalizadas(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)
    if request.method == 'GET':
        dato = pd.DataFrame(dimensionesPersonalizadas.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)
    if request.method == 'DELETE':
        count = dimensionesPersonalizadas.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def dimensionesPersonalizadas_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = dimensionesPersonalizadas.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")



#Valores unitarios
######################################################

@api_view([ 'POST','PUT','GET','DELETE'])
def preciosUnitarios_CRUD(request, id=0):
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = preciosUnitarios(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)
    if request.method == 'GET':
        dato = pd.DataFrame(preciosUnitarios.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)
    if request.method == 'DELETE':
        count = preciosUnitarios.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)
    


@api_view(['DELETE'])
def preciosUnitarios_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = preciosUnitarios.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")




@api_view([ 'POST','PUT','GET','DELETE'])
def datosIniciales_CRUD(request, id=0):
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = datosIniciales(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)
    if request.method == 'GET':
        dato = pd.DataFrame(datosIniciales.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)
    if request.method == 'DELETE':
        count = datosIniciales.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



@api_view(['DELETE'])
def datosIniciales_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = datosIniciales.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")



##############
@api_view([ 'POST','PUT','GET','DELETE'])
def datosPrimary_CRUD(request, id=0):
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = datosPrimary(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)
    if request.method == 'GET':
        dato = pd.DataFrame(datosPrimary.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)
    if request.method == 'DELETE':
        count = datosPrimary.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



@api_view(['DELETE'])
def datosPrimary_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = datosPrimary.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")

###############################





# Calculos 
###############################################



# ref='B' in locals()
# if ref==False:
#     servicio = pd.DataFrame(servicios.objects.all().values())
#     B=list(np.repeat([float(0.01)],len(servicio.index), axis=0))
#     L=B


# def Asentamiento(B,L,aSi=False,aSc=False,aSt=False,v=False, salida=False,s1=False):    

#     servicio = pd.DataFrame(servicios.objects.all().values())    
#     estratos=pd.DataFrame(perfilGeotecnico.objects.all().values())
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())    
    
#     if len(datosI.index)>0 and len(estratos.index)>0 and len(servicio.index)>0:
        
#         F3=list(pd.to_numeric(list(servicio['fuerza3'])))
        
#         prof=pd.to_numeric(datosI['profundidadCimentacion'])[0]
#         NF=pd.to_numeric(datosI['nivelFeatico'])[0]
#         estratos=estratos.sort_values('idEstrato')
               
#         Lx=[L[i]/2 for i in range(len(L))]
#         Bx=[B[i]/2 for i in range(len(B))]        
#         product = [x*y for x,y in zip(B,L)]       
#         q=[F3[x]/product[x] for x in range(len(product))]      

#         u=pd.to_numeric(list(estratos['relacionPoisson']))
#         Es=pd.to_numeric(list(estratos['moduloElasticidad']))
#         Cc=pd.to_numeric(list(estratos['indiceDeComprension']))
#         Cr=pd.to_numeric(list(estratos['indiceDeExpansibilidad']))
#         Sp=pd.to_numeric(list(estratos['presionDepreconsolidacion']))
#         e0=pd.to_numeric(list(estratos['relacionDeVacioInicial']))
#         hj=pd.to_numeric(list(estratos['espesorEstrato']))
#         Gama=pd.to_numeric(list(estratos['pesoEspecifico']))
               
#         ZI=np.arange(pd.to_numeric(datosI['h'])[0],sum(hj),pd.to_numeric(datosI['h'])[0])
#         hi=np.repeat([float(0)],len(ZI), axis=0)
#         hi[0]=ZI[0]
#         for i in range(1,len(ZI)):
#             hi[i]=pd.to_numeric(datosI['h'])[0]
        
#         Zcarg=[ZI[i]-prof for i in range(len(ZI))]
#         for i in range(len(Zcarg)):
#             if Zcarg[i]<0:
#                 Zcarg[i]=0

        
#         Crr=np.repeat([float(0)],len(ZI), axis=0)
#         Ccc=np.repeat([float(0)],len(ZI), axis=0)
#         SP=np.repeat([float(0)],len(ZI), axis=0)
#         E0=np.repeat([float(0)],len(ZI), axis=0)
#         Hj=np.repeat([float(0)],len(ZI), axis=0)
#         GAMA=np.repeat([float(0)],len(ZI), axis=0)
#         ES=np.repeat([float(0)],len(ZI), axis=0)
#         uu=np.repeat([float(0)],len(ZI), axis=0)
         
#         h=np.repeat([float(0)],len(hj), axis=0)
#         h[0]=hj[0]
#         for i in range(1,len(hj)):
#             h[i]=h[i-1]+hj[i]

        
#         s=0
#         for i in range(len(ZI)):           
            
#             if ZI[i]<=h[s]:
#                 Crr[i]=Cr[s]
#                 Ccc[i]=Cc[s]                    
#                 E0[i]=e0[s]
#                 GAMA[i]=Gama[s]
#                 SP[i]=Sp[s]
#                 Hj[i]=hj[s]
#                 ES[i]=Es[s]
#                 uu[i]=u[s]
#             else:                
#                 s=s+1
#                 Crr[i]=Cr[s]
#                 Ccc[i]=Cc[s]                    
#                 E0[i]=e0[s]
#                 GAMA[i]=Gama[s]
#                 SP[i]=Sp[s]
#                 Hj[i]=hj[s]
#                 ES[i]=Es[s]
#                 uu[i]=u[s]
               
        
#         for i in range(len(ZI)):
#             if ZI[i]<=prof:                    
#                 Ccc[i]=0
#                 Crr[i]=0
        
#         iSz=[hi[i]*GAMA[i] for i in range(len(hi))]
#         Sz=np.repeat([float(0)],len(iSz), axis=0)
#         Sz[0]=iSz[0]
#         for i in range(1,len(Sz)):
#             Sz[i]=Sz[i-1]+iSz[i]
        
#         gw=9.81
#         hw=[ZI[i]-NF for i in range(len(ZI))]
#         for i in range(len(hw)):
#             if hw[i]<0:
#                 hw[i]=0
#         U=[gw*x for x in hw]
        

#         #esfuerzos efectivos

#         Szp=[Sz[i]-U[i] for i in range(len(U))]
#         R1=np.repeat([float(0)],len(Zcarg), axis=0)
#         R2=np.repeat([float(0)],len(Zcarg), axis=0)
#         R3=np.repeat([float(0)],len(Zcarg), axis=0)
#         dsz=np.repeat([float(0)],len(Zcarg), axis=0)
#         dsx=np.repeat([float(0)],len(Zcarg), axis=0)
#         dsy=np.repeat([float(0)],len(Zcarg), axis=0)
#         DU=np.repeat([float(0)],len(Zcarg), axis=0)
#         dszp=np.repeat([float(0)],len(Zcarg), axis=0)
#         dsxp=np.repeat([float(0)],len(Zcarg), axis=0)
#         dsyp=np.repeat([float(0)],len(Zcarg), axis=0)
#         Dz=np.repeat([float(0)],len(Zcarg), axis=0)

#         ASI=np.repeat([float(0)],len(B), axis=0)
#         ASC=np.repeat([float(0)],len(B), axis=0) 
#         AST=np.repeat([float(0)],len(B), axis=0) 
#         for j in range(len(B)):
#             for k in range(len(Zcarg)):
#                 R1[k]=math.sqrt(Lx[j]**2+Zcarg[k]**2)
#                 R2[k]=math.sqrt(Bx[j]**2+Zcarg[k]**2)
#                 R3[k]=math.sqrt(Lx[j]**2+Bx[j]**2+Zcarg[k]**2)
#                 if Zcarg[k]==0:
#                     dsz[k]=0
#                     dsx[k]=0
#                     dsy[k]=0
#                 else:

#                     dsz[k]=4*(q[j]/(2*math.pi))*(math.atan(((Lx[j])*(Bx[j]))/(Zcarg[k]*R3[k]))+(((Lx[j])*(Bx[j])*Zcarg[k])/R3[k])*((1/(R1[k]**2))+(1/(R2[k]**2))))
#                     dsx[k]=4*(q[j]/(2*math.pi))*(math.atan((((Lx[j]*Bx[j])/(Zcarg[k]*R3[k]))))- (Lx[j]*Bx[j]*Zcarg[k])/((R1[k]**2)*R3[k]))
#                     dsy[k]=4*(q[j]/(2*math.pi))*(math.atan((((Lx[j]*Bx[j])/(Zcarg[k]*R3[k]))))- (Lx[j]*Bx[j]*Zcarg[k])/((R2[k]**2)*R3[k]))
                    

                
#                 DU[k]=(dsz[k]+dsy[k]+dsx[k])/3
#                 Dz[k]=((dsz[k]/ES[k])-(uu[k]/ES[k])*(dsx[k]+dsy[k]))*hi[k]

#                 dszp[k]=dsz[k]-DU[k]
#                 dsxp[k]=dsx[k]-DU[k]
#                 dsyp[k]=dsy[k]-DU[k]


       
#             Szfp=[Szp[i]+dszp[i] for i in range(len(dszp))]
        
        
         
#             Sc=np.repeat([float(0)],len(Szfp), axis=0)    
#             for k in range(len(Szfp)):
#                 if datosI['OCR'][0]=="Si":                 
#                    C1=hi[k]/(1+E0[k])
#                    term1=Ccc[k]*C1
#                    log=math.log10(Szfp[k]/Szp[k])
#                    Sc[k]=term1*log
#                 else:
#                     if SP[k]>Szfp[k] and Szfp[k]/Szp[k]>0:
#                        C1=hi[k]/(1+E0[k])
#                        term1=Crr[k]*C1
#                        log=math.log10(Szfp[k]/Szp[k])
#                        Sc[k]=term1*log
#                     elif SP[k]>Szp[k] and SP[k]<Szfp[k]:
#                        C1=hi[k]/(1+E0[k])
#                        Sc[k]=C1*(Crr[k]*math.log10(SP[k]/Szp[k]) + Ccc[k]*math.log10(Szfp[k]/SP[k]))
#                     elif Szfp[k]/Szp[k]>0:
#                         C1=hi[k]/(1+E0[k])
#                         term1=Ccc[k]*C1
#                         log=math.log10(Szfp[k]/Szp[k])
#                         Sc[k]=term1*log
#                     else:
#                         Sc[k]=0

                                 
#             ASI[j]=sum(Dz)
#             ASC[j]=sum(Sc)
#             AST[j]=sum(Dz)+sum(Sc)
        
        
#         data=pd.DataFrame({'q':q[0],'L':L[0],'B':B[0],'hi':hi,'Gama':GAMA,'ZI':ZI,'iSz':iSz,'Sz':Sz,'gw':gw,'hw':hw,'U':U,'Szp':Szp,'Zcarg':Zcarg,'R1':R1,'R2':R2,'R3':R3,'dsz':dsz,'dsx':dsx,'dsy':dsy,'DU':DU,'Dz':Dz,'dszp':dszp,'Szfp':Szfp,'Sc':Sc})
        
#         data2=pd.DataFrame({'joint':servicio['joints'],'B_Asentamiento':B,'L_Asentamiento':L,'AsentamientoInmediatos':ASI,'AsentamientoporConsolidacion':ASC,'Asentamientototal':AST}) 

#         if salida==True:
#             return data
#         if s1==True:
#             return data2


#         if aSi==True:
#             return ASI
#         elif aSc==True:
#             return ASC
#         elif aSt==True:
#             return  AST      
        
#         return JsonResponse(data2.to_dict('records'),safe=False)

#     else:
#         if v==True:
#             return pd.DataFrame({'B_Asentamiento':[0]})
#         return JsonResponse({})





# def Asentamiento2(s=True):
#     if sum(Asentamiento(B,L,aSt=True,v=True))>0:
#         datosI=pd.DataFrame(datosIniciales.objects.all().values())
#         servicio = pd.DataFrame(servicios.objects.all().values()) 
#         maxAsen=pd.to_numeric(datosI['asentamimientoMaximo'])[0] 
#         Asen_total=Asentamiento(B,L,aSt=True)    
#         for i in range(len(Asen_total)):
#             while Asen_total[i]>maxAsen:
#                 B[i]=B[i]+pd.to_numeric(datosI['aumentoB'])[0] 
#                 L[i]=L[i]+pd.to_numeric(datosI['aumentoB'])[0] 
#                 Asen_total= Asentamiento(B,L,aSt=True)
        
#         data=Asentamiento(B,L,s1=True) 
#         if s==True:
#             return data
        
#         return JsonResponse(data.to_dict('records'),safe=False)

#     else:
#         if s==True:
#             return JsonResponse(pd.DataFrame({'B_Asentamiento':[0],'L_Asentamiento':[0]}).to_dict('records'),safe=False)
#         return JsonResponse({})
        

# def AsentRegister(salida=False):
      
#     bAse=Asentamiento2(s=True)
#     if sum(bAse['B_Asentamiento'])>0:
#         B=bAse['B_Asentamiento']
#         L=bAse['L_Asentamiento']
#         datos=Asentamiento(B,L,salida=True)
#         return JsonResponse(datos.to_dict('records'),safe=False)
#     else:
#         if salida==True:
#             return [0]

#         return JsonResponse({})


# def dimensionMayor(s=False):
#     bAsen=Asentamiento2(s=True) 
#     if sum(list(Cap(bcp=True)))>0 and sum(bAsen['B_Asentamiento'])>0:
#         Bcp=list(Cap(bcp=True))              
#         bAsen=list(bAsen['B_Asentamiento'])
#         Bmayor=np.repeat([float(0)],len(Bcp), axis=0)
#         for i in range(len(Bcp)):
#             if Bcp[i]>=bAsen[i]:
#                 Bmayor[i]=Bcp[i]
#             else:
#                 Bmayor[i]=bAsen[i]
#         B=Bmayor
#         L=Bmayor
#         Asen=Asentamiento(B,L,s1=True)
#         resultado=pd.DataFrame({'DimensionMayor':Bmayor,'A_Elastico':Asen['AsentamientoInmediatos'],'A_consolidacion':Asen['AsentamientoporConsolidacion'],'A_Total':Asen['Asentamientototal']})
#         if s==True:
#             return resultado
#         return JsonResponse(resultado.to_dict('records'),safe=False)
#     else:
#         if s==True:
#             return pd.DataFrame({'DimensionMayor':[0]})
#         return JsonResponse({})





# def Excentricidades(s=True):
#     DimensionMayor=dimensionMayor(s=True)
#     servicio = pd.DataFrame(servicios.objects.all().values())
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
#     if sum(DimensionMayor['DimensionMayor'])>0:
#         F3=list(pd.to_numeric(list(servicio['fuerza3'])))
#         M1=pd.to_numeric(servicio["momento1"])
#         M2=pd.to_numeric(servicio["momento2"])
#         lado_neto=list(DimensionMayor['DimensionMayor'])
        
#         Ex=[abs(M1[i]/F3[i]) for i in range(len(F3))]
#         Ey=[abs(M2[i]/F3[i]) for i in range(len(F3))]
#         Bz=[2*(lado_neto[i]/2+abs(Ex[i])) for i in range(len(lado_neto))]
#         Lz=[2*(lado_neto[i]/2+abs(Ey[i])) for i in range(len(lado_neto))]
#         for j in range(len(F3)):
#             while Ex[j]>(1/6)*Bz[j]:
#                 Bz[j]=Bz[j]+pd.to_numeric(datosI['aumentoB'])[0]
        
#         for k in range(len(F3)):
#             while Ey[k]>(1/6)*Lz[k]:
#                 Lz[k]=Lz[k]+pd.to_numeric(datosI['aumentoB'])[0]
#         B=Bz
#         L=Lz
#         BL=Asentamiento(B,L,s1=True)
        
#         data=pd.DataFrame({'joint':servicio['joints'],'B_Excentricidad':Bz,'L_Excentricidad':Lz,'AsentamientoElastico':BL['AsentamientoInmediatos'],'AsentamientoPorConsolidacion':BL['AsentamientoporConsolidacion'],'Asentamientototal':BL['Asentamientototal']})
#         if s==True:
#             return data
#         return JsonResponse(data.to_dict('records'),safe=False)
#     else:
#         if s==True:
#             return pd.DataFrame({'B_Excentricidad':[0],'L_Excentricidad':[0]})
#         return JsonResponse({})




# def AsenD(consola=False):
#     diferencial= pd.DataFrame(diferenciales.objects.all().values())
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
#     dim=Excentricidades(s=True)

#     if sum(dim['B_Excentricidad'])>0 and len(diferencial.index)>0 and len(dim['B_Excentricidad'])>1:
#         B=list(dim['B_Excentricidad'])
#         L=list(dim['L_Excentricidad'])
#         distancias=pd.to_numeric(diferencial["distancias"])
#         joint2=pd.to_numeric(diferencial["joints"])
#         Vecinos=pd.to_numeric(diferencial["vecinos"])
#         joint=pd.to_numeric(servicio["joints"])
#         Asen_total=Asentamiento(B,L,aSt=True,v=True)
#         criterio=list(pd.to_numeric(diferencial['criterioDiferencial']))

#         DH_DIF_Adm=[distancias[x]/criterio[x] for x in range(len(distancias))]
#         DH_DIF_Cal=np.repeat([float(0)],len(joint2), axis=0)
#         for i in range(len(joint2)):
#             DH_DIF_Cal[i]=float(abs((Asen_total[int(np.where([joint[x]==joint2[i] for x in range(len(joint))])[0])]-Asen_total[int(np.where([joint[x]==Vecinos[i] for x in range(len(joint))])[0])])*1000))

        
#         for i in range(len(DH_DIF_Adm)):
#             while  DH_DIF_Adm[i]<DH_DIF_Cal[i]:
#                 Joi1=joint[int(np.where([joint[x]==joint2[i] for x in range(len(joint))])[0])]
#                 Joi2=joint[int(np.where([joint[x]==Vecinos[i] for x in range(len(joint))])[0])]
#                 A1=Asen_total[int(np.where([joint[x]==joint2[i] for x in range(len(joint))])[0])]
#                 A2=Asen_total[int(np.where([joint[x]==Vecinos[i] for x in range(len(joint))])[0])]
#                 if A1>A2:
#                     B[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]=B[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
#                     L[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]=L[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
                    
#                 elif A2>A1:
#                     B[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]=B[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
#                     L[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]=L[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
                    
#                 elif A1==A2:
#                     B[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]=B[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
#                     B[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]=B[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
#                     L[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]=L[int(np.where([joint[x]==Joi1 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
#                     L[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]=L[int(np.where([joint[x]==Joi2 for x in range(len(joint))])[0])]+pd.to_numeric(datosI['aumentoB'])[0] 
                
                
#                 Asen_total=Asentamiento(B,L,aSt=True,v=True)            
#                 DH_DIF_Cal[i]=abs(A1-A2)*1000
        
#         ASI=Asentamiento(B,L,aSi=True,v=True)
#         ASC=Asentamiento(B,L,aSc=True,v=True)
#         AST=Asentamiento(B,L,aSt=True,v=True)
#         data= pd.DataFrame({'B':B,'L':L,'A_Elastico':ASI,'A_consolidacion':ASC,'A_Total':AST})
#         if consola==True:
#             return  data
        
#         return JsonResponse(data.to_dict('records'), safe=False)

#     else:
#         if consola==True:
#             return  pd.DataFrame({'B':[0],'L':[0]})
            
#         return JsonResponse({})          




# def dimensionesConstruccion(s=True):
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
#     Personalizado=pd.DataFrame(dimensionesPersonalizadas.objects.all().values())
#     BL=AsenD(consola=True)
#     dim=Excentricidades(s=True)
#     Be=dim['B_Excentricidad']
#     Le=dim['L_Excentricidad']
#     Bd=list(BL['B'])
#     Ld=list(BL['L'])
#     if sum(Bd)>0 or sum(Be)>0:
#         if sum(Be)>0 and len(Be)==1:
#             B=Be
#             L=Le
#             B_Const=[round(B[i],int(datosI['cifrasDecimales'][0])) for i in range(len(B))]
#             L_Const=[round(L[i],int(datosI['cifrasDecimales'][0])) for i in range(len(L))]
#         elif sum(Bd)>0 and len(Bd)>1:
#             B=Bd
#             L=Ld
#             B_Const=[round(B[i],int(datosI['cifrasDecimales'][0])) for i in range(len(B))]
#             L_Const=[round(L[i],int(datosI['cifrasDecimales'][0])) for i in range(len(L))]
           
#         else:
#             dimBL=Asentamiento2(s=True)
#             B=list(dimBL['B_Asentamiento'])
#             L=list(dimBL['L_Asentamiento'])
#             B_Const=[round(B[i],int(datosI['cifrasDecimales'][0])) for i in range(len(B))]
#             L_Const=[round(L[i],int(datosI['cifrasDecimales'][0])) for i in range(len(L))]



            
        
#         for i in range(len(B_Const)):
#             if B_Const[i]<1:
#                 B_Const[i]=1
#             if L_Const[i]<1:
#                 L_Const[i]=1
#             if B_Const[i]<B[i]:
#                 B_Const[i]=B_Const[i]+pd.to_numeric(datosI['salto'])[0]
#             if L_Const[i]<L[i]:
#                 L_Const[i]=L_Const[i]+pd.to_numeric(datosI['salto'])[0]

        
#         if len(Personalizado.index)>0 and datosI['personalizado'][0]=='si':
#             B_Const=list(pd.to_numeric(Personalizado['bpersonalizado']))
#             L_Const=list(pd.to_numeric(Personalizado['lpersonalizado']))

#         ASI=Asentamiento(B_Const,L_Const,aSi=True,v=True)
#         ASC=Asentamiento(B_Const,L_Const,aSc=True,v=True)
#         AST=Asentamiento(B_Const,L_Const,aSt=True,v=True)
        
#         data=pd.DataFrame({'B_construcion':B_Const,'L_construcion':L_Const,'Asen_Elastico':ASI,'Asen_consolidacion':ASC,'Asen_Total':AST})
#         if s==True:
#             return data
        
#         return JsonResponse(data.to_dict('records'), safe=False)
#     else:
#         if s==True:
#             return pd.DataFrame({'B_construcion':[0],'L_construcion':[0]})
#         return JsonResponse({})




# def cant_ObraPres(R=False):
#     ValoresUnitarios=pd.DataFrame(preciosUnitarios.objects.all().values())
#     Personalizado=pd.DataFrame(dimensionesPersonalizadas.objects.all().values())
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
#     datosII=pd.DataFrame(datosPrimary.objects.all().values())

#     BL=dimensionesConstruccion(s=True) 

#     if len(ValoresUnitarios.index)>0 and len(datosI.index)>0 and len(datosII.index)>0:
#         if sum(BL['B_construcion'])>0 and datosI['personalizado'][0]=='no':
#             B=BL['B_construcion']
#             L=BL['L_construcion']
#         elif sum(BL['B_construcion'])>=0 and  datosI['personalizado'][0]=='si' and len(Personalizado.index)>0:
#             B=list(pd.to_numeric(Personalizado['bpersonalizado']))
#             L=list(pd.to_numeric(Personalizado['lpersonalizado']))
#         else:
#             return JsonResponse({})
        

#         prof=pd.to_numeric(datosI['profundidadCimentacion'])[0]   
#         EspesorSolado=pd.to_numeric(datosII['espesorSolado'])[0]
        
#         #Excavación
#         Area=list(np.multiply(list(B),list(L)))
#         Exc=[(prof+EspesorSolado)*i for i in Area]

#         #Solado
#         solado=[EspesorSolado*x for x in Area]

#         #Concreto Zapatas
#         MayorBL=np.repeat([float(0)],len(B), axis=0)
#         MenorBL=np.repeat([float(0)],len(B), axis=0)
#         for i in range(len(B)):
#             if  B[i]>=L[i]:
#                 MayorBL[i]=B[i]
#                 MenorBL[i]=L[i]
#             if L[i]>B[i]:
#                 MayorBL[i]=L[i]
#                 MenorBL[i]=B[i]

#         if datosII['condicion'][0]=='mayorBL':
#             ConcZ=np.multiply([pd.to_numeric(datosII['espesorZapata'])[0]*x for x in MayorBL], Area)
#             ConcP=[prof*pow(pd.to_numeric(datosII['espesorPedestal'])[0]*MayorBL[i],2) for i in range(len(MayorBL))]

#         elif datosII['condicion'][0]=='menorBL':
#             ConcZ=np.multiply([pd.to_numeric(datosII['espesorZapata'])[0]*x for x in MenorBL], Area)
#             ConcP=[prof*pow(pd.to_numeric(datosII['espesorPedestal'])[0]*MenorBL[i],2) for i in range(len(MenorBL))]
#         else:
#             ConcZ=np.multiply([pd.to_numeric(datosII['espesorZapata'])[0]*x for x in MenorBL], Area)
#             ConcP=[prof*pow(pd.to_numeric(datosII['espesorPedestal'])[0]*MenorBL[i],2) for i in range(len(MenorBL))]

        
#         Relleno=[Exc[i]-solado[i]-ConcP[i]-ConcZ[i] for i in range(len(B))]
#         for i in range(len(Relleno)):
#             if Relleno[i]<=0:
#                 Relleno[i]=0

#         #Presupuesto
    
#         PrecioExc=pd.to_numeric(ValoresUnitarios['precioExc'])[0]/1000000
#         VPExc=[i*PrecioExc for i in Exc]

#         PrecioConcZ=pd.to_numeric(ValoresUnitarios['precioConcZ'])[0]/1000000
#         VPConcZ=[i*PrecioConcZ for i in ConcZ]

#         PrecioConcP=pd.to_numeric(ValoresUnitarios['precioConcP'])[0]/1000000
        
#         VPConcP=[i*PrecioConcP for i in list(ConcP)]

#         PrecioRelleno=pd.to_numeric(ValoresUnitarios['precioRelleno'])[0]/1000000
        
#         VPRelleno=[i*PrecioRelleno for i in Relleno]

#         Preciosolado=pd.to_numeric(ValoresUnitarios['preciosolado'])[0]/1000000
#         VPsolado=[i*Preciosolado for i in solado]
#         AceroZapata=[pd.to_numeric(datosII['cuantiaAceroZapata'])[0]*i for i in ConcZ]
#         AceroPedestal=[pd.to_numeric(datosII['cuantiaAceroPedestal'])[0]*i for i in ConcP]
#         PrecioAceroZapata=pd.to_numeric(ValoresUnitarios['precioAceroZapata'])[0]/1000000
#         PrecioAceroPedestal=pd.to_numeric(ValoresUnitarios['precioAceroPedestal'])[0]/1000000
#         VpAceroZapata=[PrecioAceroZapata*i for i in AceroZapata]
#         VpAceroPedestal=[PrecioAceroPedestal*i for i in AceroPedestal]
#         VPZapata=[VPExc[i]+VPConcZ[i] +VPConcP[i]+VPRelleno[i]+VPsolado[i]+VpAceroZapata[i]+VpAceroPedestal[i] for i in range(len(B))]
#         SubTotal=sum(VPZapata)
#         data=pd.DataFrame({'Profundidad':prof,'B':[round(i,1) for i in B],'L':[round(i,1) for i in L],'Excavacion':[round(i,1) for i in Exc],'solado':[round(i,2) for i in solado],'ConcretoZapata':[round(i,2) for i in ConcZ],'ConcretoPedestal':[round(i,2) for i in ConcP],'Relleno':[round(i,2) for i in Relleno],'ValorParcialExcavacion':[round(i,2) for i in VPExc],'ValorParcialSolado':[round(i,2) for i in VPsolado],'ValorParcialConcretoZapata':[round(i,2) for i in VPConcZ],'ValorParcialConcretoPedestal':[round(i,2) for i in VPConcP],'VPRelleno':[round(i,2) for i in VPRelleno],'AceroZapata':[round(i,2) for i in AceroZapata],'AceroPedestal':[round(i,2) for i in AceroPedestal],'VpAceroZapata':[round(i,2) for i in VpAceroZapata],'VpAceroPedestal':[round(i,2) for i in VpAceroPedestal],'VPZapata':[round(i,2) for i in VPZapata],'SubTotal':round(SubTotal,1)})
        
#         if R==True:
#             return data
        
#         return JsonResponse(data.to_dict('records'), safe=False)
    
#     else:
#         if R==True:
#             return  pd.DataFrame({'B':[0],'L':[0]})
#         return JsonResponse({}) 
    



# def verificar(s=True):
#     diferencial= pd.DataFrame(diferenciales.objects.all().values())
#     Personalizado=pd.DataFrame(dimensionesPersonalizadas.objects.all().values())
#     if len(Personalizado.index)>0 and len(diferencial.index)>0:

#         BL=dimensionesConstruccion(s=True) 
        
    
#         B=BL['B_construcion']
#         L=BL['L_construcion']


        
#         Ase_total=Asentamiento(B,L,aSt=True,v=True)
#         distancias=list(pd.to_numeric(diferencial['distancias']))
#         joint2=list(pd.to_numeric(diferencial['joints']))
#         joint=list(pd.to_numeric(servicio['joints']))
#         criterio=list(pd.to_numeric(diferencial['criterioDiferencial']))
#         Vecinos=list(pd.to_numeric(diferencial['vecinos']))
#         DH_DIF_Adm=[distancias[x]/criterio[x] for x in range(len(distancias))]
#         DH_DIF_Cal=np.repeat([float(0)],len(joint2), axis=0)
#         for i in range(len(joint2)):
#             DH_DIF_Cal[i]=float(abs((Ase_total[int(np.where([joint[x]==joint2[i] for x in range(len(joint))])[0])]-Ase_total[int(np.where([joint[x]==Vecinos[i] for x in range(len(joint))])[0])])*1000))

#         prueba=np.repeat([0],len(joint2), axis=0)
            
#         for i in range(len(DH_DIF_Cal)):
#             if DH_DIF_Adm[i]<DH_DIF_Cal[i]:
#                 prueba[i]=1
#             else:
#                 prueba[i]=0
#         print(prueba)
#         if sum(prueba)>0:
#             return JsonResponse(pd.DataFrame({'Cumple':['No cumple con asentamientos diferenciales']}).to_dict('records'),safe=False)
#         else:
#             return JsonResponse(pd.DataFrame({'Cumple':['Si cumple con asentamientos diferenciales']}).to_dict('records'),safe=False)
#     else:
#         return JsonResponse({})


# def AsentamiendoPersonalizado(s=True):
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
#     Personalizado=pd.DataFrame(dimensionesPersonalizadas.objects.all().values())
#     BL=Asentamiento2(s=True)
#     if len(Personalizado.index)>0 and sum(BL['B_Asentamiento'])>0 and datosI['personalizado'][0]=='si':
#         B=list(pd.to_numeric(Personalizado['bpersonalizado']))
#         L=list(pd.to_numeric(Personalizado['lpersonalizado']))
#         ASI=Asentamiento(B,L,aSi=True,v=True)
#         ASC=Asentamiento(B,L,aSc=True,v=True)
#         AST=Asentamiento(B,L,aSt=True,v=True)
#         data=pd.DataFrame({'B_pers':B,'L_pers':L,'AsenInmediato':ASI,'AsenConsolidacion':ASC,'AsenTotal':AST})
#         if s==True:
#             return data        
#         return JsonResponse(data.to_dict('records'),safe=False)
#     else:
#         if s==True:
#             return pd.DataFrame({'B_pers':[0],'L_pers':[0]})

  

# def secuencia(R=False):
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
#     servicio = pd.DataFrame(servicios.objects.all().values())
#     dim=cant_ObraPres(R=True)
#     AsenBL=Asentamiento2(s=True)
#     dim2=Excentricidades(s=True)   

#     if sum(list(Cap(bcp=True,p=True)))>0 and len(servicio.index)>0 and len(datosI.index)>0 and sum(list(dim['B']))>0 and sum(list(AsenBL['B_Asentamiento']))>0:
#         prof=pd.to_numeric(datosI['profundidadCimentacion'])[0]
#         Bcp=list(Cap(bcp=True,p=True))
#         qa=Cap(portante=True,p=True)        
#         bAsen=list(AsenBL['B_Asentamiento'])
#         Be=dim2['B_Excentricidad']
#         Le=dim2['L_Excentricidad']

#         difBL=AsenD(consola=True)
#         if sum(difBL['B'])>0 and len(Le)>1:
#             Bd=difBL['B']
#             Ld=difBL['L']
#         else:
#             Bd=''
#             Ld=''

#         Bf=list(dim['B'])
#         Lf=list(dim['L'])

        
#         F3=list(pd.to_numeric(list(servicio['fuerza3'])))

#         AsenTotal=Asentamiento(B,L,aSt=True,v=True)

#         KzAct=[[F3[i]/[B[j]*L[j] for j in range(len(B))][i] for i in range(len(F3)) ][x]/AsenTotal[x] for x in range(len(F3))]
#         KzAdm=[qa[i]/AsenTotal[i] for i in range(len(AsenTotal))]
          
           
        
#         datos=pd.DataFrame({'profundidad':prof,'BCP':[round(i, 3) for i in Bcp],'qa':[round(i, 3) for i in qa],'KzAct':[round(i, 3) for i in KzAct],'KzAdm':[round(i, 3) for i in KzAdm], 'AsenB':[round(i, 3) for i in bAsen],'B_Ex':[round(i, 3) for i in Be],'L_Ex':[round(i, 3) for i in Le],'Bdif':[round(i,3) for i in Bd],'Ldif':[round(i,3) for i in Ld], 'BConstr':Bf,'LConstr':Lf})
        
#         if R==True:
#             return datos
        
#         return JsonResponse(datos.to_dict('records'), safe=False)
    
#     else:
#         if R==True:
#             return "no existe informacion"
#         JsonResponse({})
    


# #Presupuesto


# def presupuesto(r=False):
#     ValoresUnitarios=pd.DataFrame(preciosUnitarios.objects.all().values())
#     data1=cant_ObraPres(R=True)

#     if len(ValoresUnitarios.index)>0 and sum(data1['B'])>0:
#         precioExc=pd.to_numeric(list(ValoresUnitarios['precioExc']))
#         precioConcZ=pd.to_numeric(ValoresUnitarios['precioConcZ'])
#         precioConcP=pd.to_numeric(ValoresUnitarios['precioConcP'])
#         precioRelleno=pd.to_numeric(ValoresUnitarios['precioRelleno'])
#         preciosolado=pd.to_numeric(ValoresUnitarios['preciosolado'])
#         precioAceroZapata=pd.to_numeric(ValoresUnitarios['precioAceroZapata'])
#         precioAceroPedestal=pd.to_numeric(ValoresUnitarios['precioAceroPedestal'])

#         Exc=[sum(list(data1['Excavacion']))]
#         solado=[sum(list(data1['solado']))]
#         ConcZ=[sum(list(data1['ConcretoZapata']))]
#         ConcP=[sum(list(data1['ConcretoPedestal']))]
#         Relleno=[sum(list(data1['Relleno']))]
#         VPExc=[sum(list(data1['ValorParcialExcavacion']))]
#         VPsolado=[sum(list(data1['ValorParcialSolado']))]
#         VPConcZ=[sum(list(data1['ValorParcialConcretoZapata']))]
#         VPConcP=[sum(list(data1['ValorParcialConcretoPedestal']))]
#         VPRelleno=[sum(list(data1['VPRelleno']))]
#         AceroZapata=[sum(list(data1['AceroZapata']))]
#         AceroPedestal=[sum(list(data1['AceroPedestal']))]
#         VpAceroZapata=[sum(list(data1['VpAceroZapata']))]
#         VpAceroPedestal=[sum(list(data1['VpAceroPedestal']))]
#         VPZapata=[sum(list(data1['VPZapata']))]
#         SubTotal=[sum(list(data1['SubTotal']))]
#         data=pd.DataFrame({'Exc':Exc,'solado':solado,'ConcZ':ConcZ,'ConcP':ConcP,'Relleno':Relleno,'VPExc':VPExc,'VPsolado':VPsolado,'VPConcZ':VPConcZ,'VPConcP':VPConcP,'VPRelleno':VPRelleno,'AceroZapata':AceroZapata,'AceroPedestal':AceroPedestal, 'VpAceroZapata':VpAceroZapata,'VpAceroPedestal':VpAceroPedestal,'VPZapata':VPZapata,'SubTotal':SubTotal})

            
#         data2=pd.DataFrame({'precioExc':precioExc,'precioConcZ':precioConcZ,'precioConcP':precioConcP,'precioRelleno':precioRelleno,'preciosolado':preciosolado,'precioAceroZapata':precioAceroZapata,'precioAceroPedestal':precioAceroPedestal})
#         data3=pd.concat([data2,data], axis=1,)
       

#         return JsonResponse(data3.to_dict('records'), safe=False)
#     else:
#         return JsonResponse({})














































