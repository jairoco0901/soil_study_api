from django.contrib import admin
from . import models
from typing import OrderedDict

admin.site.register(models.servicios),
admin.site.register(models.diferenciales),
admin.site.register(models.preciosUnitarios),
admin.site.register(models.datosIniciales),
admin.site.register(models.datosPrimary),
admin.site.register(models.dimensionesPersonalizadas),




