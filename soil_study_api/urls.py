"""soil_study_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url, include 
from .import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/',include('soil_study_api.users.urls')),
    path('perforation-register/', include('soil_study_api.perforation_register.urls')),
    path('geotechnical-profile/', include('soil_study_api.Geotechnical_profile.urls')),
    path('laboratorios/',include('soil_study_api.laboratories.urls')),
    path('capacidad-portante/',include('soil_study_api.portant_capacity.urls')),
    path('geologia/',include('soil_study_api.geology.urls')),
    path('imagesGeo/',include('soil_study_api.geo_images.urls')),
    path('Asentamiento/',include('soil_study_api.settlements.urls')),
    path('projectos/',include('soil_study_api.Projects.urls')),
    path('Home/',include('soil_study_api.Home1.urls')),
    # url(r'^', include('soil_study_api.projectos.urls')),
    
]
