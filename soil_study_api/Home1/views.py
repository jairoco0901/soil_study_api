from django.http import JsonResponse, HttpResponse
import json
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict
from soil_study_api.Home1.models import home1

import pandas as pd

from django.core import serializers



@api_view([ 'POST'])
def home_post_CRUD(request, id=0):
    if request.method == "PUT":
        if id==0:
            upddato = home1.objects.get(pk=id)
            form = serialize(instance=upddato)
            return JsonResponse(form)
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = home1(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)

# @api_view(['GET'])
# def home_get_CRUD(request):
#     if request.method == 'GET':
#         dato = pd.DataFrame(home.objects.all().values())
#         return JsonResponse(dato.to_dict('records'), safe=False)


# @api_view(['DELETE'])
# def delete_all_home_CRUD(request):
#     #metodo para borrar todos los registros de sondeos
#     if request.method == 'DELETE':
#         count = home.objects.all().delete()
#         return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)
