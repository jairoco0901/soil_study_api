from rest_framework import serializers

from models import perfilGeotecnico


class perforationRegisterCRUD(serializers.ModelSerializer):
    class Meta:
        model = perfilGeotecnico
        fields = '__all__'
