from django.urls import path

from . import views

urlpatterns = [
    path('perfilGeotecnico_CRUD', views.perfilGeotecnico_CRUD),
    path('perfilGeotecnico-delete/<str:id>', views.perfilGeotecnico_delete_CRUD),
    
    path('Perfil', views.Perfil),  
    
]