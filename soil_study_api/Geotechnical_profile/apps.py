from django.apps import AppConfig


class UsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'soil_study_api.Geotechnical_profile'
    app_label = 'perfil geotecnico'