# Generated by Django 3.2.3 on 2021-11-03 13:57

from django.db import migrations, models



class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
       
        migrations.CreateModel(
            name='perfilGeotecnico',
            fields=[
                ('idEstrato', models.CharField(primary_key=True, max_length=10, editable=True)),
                ('espesorEstrato', models.DecimalField(max_digits=10, decimal_places=3,verbose_name='Espesor')),
                ('relacionPoisson',  models.DecimalField(max_digits=10, decimal_places=3)),
                ('indiceDeComprension', models.DecimalField(max_digits=10, decimal_places=3, verbose_name='Cc')),
                ('pesoEspecifico', models.DecimalField(max_digits=10, decimal_places=3)),
                ('moduloElasticidad', models.DecimalField(max_digits=10, decimal_places=3, verbose_name='Es')),
                ('relacionDeVacioInicial', models.DecimalField(max_digits=10, decimal_places=3, verbose_name='e0')),
                ('presionDepreconsolidacion', models.DecimalField(max_digits=10, decimal_places=3,verbose_name='Pc')),
                ('indiceDeExpansibilidad', models.DecimalField(max_digits=10, decimal_places=3,verbose_name='Cs')),
                ('cohesionSuelo',  models.DecimalField(max_digits=10, decimal_places=4,verbose_name='Cohesion')),
                ('friccionSuelo',   models.DecimalField(max_digits=10, decimal_places=4,verbose_name='Friccion')),


            ],
        ),
        
    ]
