from rest_framework import serializers

from models import  laboratory


class perforationRegisterCRUD(serializers.ModelSerializer):
    class Meta:
        model = laboratory
        fields = '__all__'
