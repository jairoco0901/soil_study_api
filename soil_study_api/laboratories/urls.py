from django.urls import path

from . import views


urlpatterns = [
    path('laboratory-post', views.laboratory_post_CRUD),
    path('laboratory-get', views.laboratory_get_CRUD),
    path('laboratory-delete', views.laboratory_delete_CRUD),
    path('laboratory-delete-all', views.delete_all_laboratory_CRUD),

]
