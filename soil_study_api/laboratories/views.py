from django.http import JsonResponse, HttpResponse
import json
from rest_framework.response import Response
from rest_framework import status
#from rest_framework.authentication import BasicAuthentication
#from soil_study_api.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict
from soil_study_api.laboratories.models import laboratory
import pandas as pd

from django.core import serializers


############################################################################################



@api_view([ 'POST'])
def laboratory_post_CRUD(request, id=0):
    if request.method == "PUT":
        if id==0:
            upddato = laboratory.objects.get(pk=id)
            form = serialize(instance=upddato)
            return JsonResponse(form)
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = laboratory(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)






@api_view(['GET'])
def laboratory_get_CRUD(request):
    if request.method == 'GET':
        laboratorio = pd.DataFrame(laboratory.objects.all().values())
        return JsonResponse(laboratorio.to_dict('records'), safe=False)



@api_view(['DELETE'])
def delete_all_laboratory_CRUD(request):
    #metodo para borrar todos los registros de sondeos
    if request.method == 'DELETE':
        count = laboratory.objects.all().delete()
        return JsonResponse({'message': '{}were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def laboratory_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = laboratory.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")



