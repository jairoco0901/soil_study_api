from django.apps import AppConfig


class UsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'soil_study_api.portant_capacity'
    app_label = 'capacidad portante'


