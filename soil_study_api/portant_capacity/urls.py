from django.urls import path

from . import views

urlpatterns = [
    path('datosIniciales-post',views.datosIniciales_post_CRUD),
    path('datosIniciales-get',views.datosIniciales_get_CRUD),
    path('datosIniciales-delete',views.datosIniciales_delete_CRUD),
    path('datosIniciales-delete-all',views.delete_all_datosIniciales_CRUD),    
   #path('Cap', views.Cap)
    
]