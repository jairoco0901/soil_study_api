from django.http import JsonResponse, HttpResponse
import json
from django.shortcuts import render
from pandas.tseries.offsets import DateOffset
#from rest_framework.authentication import BasicAuthentication
#from soil_study_api.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
#from rest_framework.authtoken.models import Token
from django.core.serializers import serialize
from django.forms.models import model_to_dict

from django.core import serializers
import pandas as pd
import numpy as np
import math
from rest_framework import status
from soil_study_api.settlements.models import datosIniciales,  dimensionesPersonalizadas, servicios
from soil_study_api.Geotechnical_profile.models import perfilGeotecnico



@api_view([ 'POST'])
def datosIniciales_post_CRUD(request, id=0):
    if request.method == "PUT":
        if id==0:
            upddato = datosIniciales.objects.get(pk=id)
            form = serialize(instance=upddato)
            return JsonResponse(form)
    if request.method == 'POST':
        datos =json.loads(request.body)
        newproject = datosIniciales(**datos)
        newproject.save()
        dato= model_to_dict(newproject)
        return JsonResponse(dato)

@api_view(['GET'])
def datosIniciales_get_CRUD(request):
    if request.method == 'GET':
        dato = pd.DataFrame(datosIniciales.objects.all().values())
        return JsonResponse(dato.to_dict('records'), safe=False)


@api_view(['DELETE'])
def delete_all_datosIniciales_CRUD(request):
    #metodo para borrar todos los registros de sondeos
    if request.method == 'DELETE':
        count = datosIniciales.objects.all().delete()
        return JsonResponse({'message': '{} were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def datosIniciales_delete_CRUD(request,id):
    if request.method == 'DELETE':
        dato = datosIniciales.objects.get(pk=id)
        dato.delete()
        return HttpResponse(dato, content_type="text/json-comment-filtered")



#####################################################################




# def Cap(portante=False,cohesion=False,Friccion=False,bcp=False,v=False,p=False):
#     estratos = pd.DataFrame(perfilGeotecnico.objects.all().values())
#     datosI=pd.DataFrame(datosIniciales.objects.all().values())
    
#     servicio=pd.DataFrame(servicios.objects.all().values())

#     if len(servicio.index)>0 and len(datosI.index)>0 and len(estratos.index)>0:

#         F3=pd.to_numeric(servicio['fuerza3'])
#         hj=pd.to_numeric(estratos['espesorEstrato'])
#         Gama=pd.to_numeric(estratos['pesoEspecifico'])
#         prof=pd.to_numeric(datosI['profundidadCimentacion'])[0]
#         modelo=datosI['modelo'][0]
#         FS=pd.to_numeric(datosI['factorResistencia'])[0]
        
#         j=0
#         q=np.repeat(float(0), [len(hj)], axis=0)
        
#         while j<len(hj):
#             q[0]=Gama[0]*prof
            
#             S=sum(hj)
            

#             if prof<=S:
#                 q[j]=q[j-1]+ hj[j]*Gama[j] +(prof-hj[j])*Gama[j]
#                 fi=pd.to_numeric(list(estratos['friccionSuelo']))[j]
#                 C=pd.to_numeric(list(estratos['cohesionSuelo']))[j]
             
               
#             j=j+1  


#         q=max(q)
#         Gama=(q/prof)
        
        
#         B=np.repeat(float(0), [len(F3)], axis=0)
#         Nq=math.exp(math.pi*math.tan(fi*(math.pi/180)))*pow(math.tan(math.pi/4+(fi/2)*(math.pi/180)),2)
#         Nc=(Nq-1)/math.tan(fi*(math.pi/180))
#         if modelo=='hansen':
#             Ny=1.5*(Nq-1)*math.tan(fi*(math.pi/180))
#         elif modelo=='Vesic':
#             Ny=2*(Nq+1)*math.tan(fi*(math.pi/180))
#         qu= [C*Nc+q*Nq+Gama*i*Ny/2 for i in B]
#         qa=[i/FS for i in qu]    

#         for i in range(len(F3)):
#             while qa[i]*pow(B[i],2)<F3[i]:
#                 qu= [C*Nc+q*Nq+Gama*i*Ny/2 for i in B]
#                 qa=[i/FS for i in qu]
#                 B[i]=B[i]+0.01
        
#         for i in range(len(B)):
#             B[i]=B[i]+0.01
        
#         Personalizado=pd.DataFrame(dimensionesPersonalizadas.objects.all().values())
#         cuantia=pd.DataFrame(datosIniciales.objects.all().values()) 

#         if p==True and len(Personalizado.index)>0 and len(cuantia.index)>0 and cuantia['personalizado'][0]=='si':
            
#             B=list(pd.to_numeric(Personalizado['bpersonalizado']))
#             Nq=math.exp(math.pi*math.tan(fi*(math.pi/180)))*pow(math.tan(math.pi/4+(fi/2)*(math.pi/180)),2)
#             Nc=(Nq-1)/math.tan(fi*(math.pi/180))
#             if modelo=='hansen':
#                 Ny=1.5*(Nq-1)*math.tan(fi*(math.pi/180))
#             elif modelo=='Vesic':
#                 Ny=2*(Nq+1)*math.tan(fi*(math.pi/180))
#             qu= [C*Nc+q*Nq+Gama*i*Ny/2 for i in B]
#             qa=[i/FS for i in qu]    

#         if portante==True:
#             return qa
#         if cohesion==True:
#             return C
        
#         if cohesion==True:
#             return C
        
#         if Friccion==True:
#             return fi

#         if bcp==True:
#             return list(B)

#         return JsonResponse(pd.DataFrame({'B':list(B)}).to_dict('records'), safe=False)
       
#     else:
#         if v==True:
#             return [0]
#         return JsonResponse({})




