from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin, make_password
from django.db.models.fields import CharField
from datetime import datetime


Roles =(("admin","admin"), ("creator", "creator"), ("sale", "sale"))

class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")

        user = self.model(email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        user = self.model(email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user


class User(AbstractUser):
    pass
    username = None
    email = models.EmailField(('email address'), unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()


class Organizacion(models.Model):
    name = models.CharField(max_length=200)



class Usuarios(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nombres')
    lastName = models.CharField(max_length=140, verbose_name='Apellidos')
    dni = models.CharField(max_length=11, unique=True,verbose_name='Dni')
    dateJoined = models.DateField(default=datetime.now, verbose_name='Fecha registro')
    avatar = models.ImageField(upload_to='logo/%Y/$m/%d', null =True, blank=True)
    cellPhone = models.CharField(max_length=15, unique=True, verbose_name='Celular')
    email = models.EmailField(max_length=254, unique=True, verbose_name='Correo Electronico')
    password = models.CharField(max_length=250)
    is_admin = models.BooleanField(default=False)
    create_date = models.DateField(auto_now_add=True)
    def __str__(self):
        return self.name
