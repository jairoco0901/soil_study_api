from django.core import serializers
from django.http import JsonResponse, HttpResponse, response
from rest_framework import status, permissions
from rest_framework.authentication import BasicAuthentication
from soil_study_api.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.authtoken.models import Token
from django.forms.models import model_to_dict
from rest_framework.views import APIView
import logging
#from drf_yasg.utils import swagger_auto_schema
from django.contrib.auth.hashers import make_password, check_password
import json
from django.contrib import auth
from soil_study_api.users.models import Organizacion, Usuarios
#from soil_study_api.users.serializers import  UserSerializerWithToken, UserSerializer



@api_view(['GET'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def login(request):
    token, created = Token.objects.get_or_create(user=request.user)
    return JsonResponse({
        "email": request.user.email,
        "first_name": request.user.first_name,
        "last_name": request.user.last_name,
        "token": token.key
    })




@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user_information(request):
    userData = model_to_dict(request.user)    
    del userData['password']
    return JsonResponse({
        "email": request.user.email,
        "first_name": request.user.first_name,
        "last_name": request.user.last_name,    
    })

# @api_view(['GET'])
# def current_user(request):
#     """
#     Determine the current user by their token, and return their data
#     """

#     serializer = UserSerializer(request.user)
#     return response(serializer.data)


# class UserList(APIView):
#     """
#     Create a new user. It's called 'UserList' because normally we'd have a get
#     method here too, for retrieving a list of all User objects.
#     """

#     permission_classes = (permissions.AllowAny,)

#     def post(self, request, format=None):
#         serializer = UserSerializerWithToken(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return response(serializer.data, status=status.HTTP_201_CREATED)
#         return response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# @swagger_auto_schema(methods=['post'], request_body=serializersUser)
# @api_view(['POST'])
# def add_user(request):
#     if request.method == 'POST':
#         pass_encrypt = make_password(request.data['password'])
#         user_data = {
#             "name": request.data['name'],
#             "lastName": request.data['lastName'],
#             "dni": request.data['dni'],
#             "email": request.data['email'],
#             "password": pass_encrypt,
#         }
#         serializer = serializersUser(data=user_data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET'])
# def all_user(request):
#     if request.method == 'GET':
#         users = Usuarios.objects.all()

#         user_name = request.GET.get('name', None)
#         if user_name is not None:
#             users = users.filter(
#                 user_name__icontains=user_name)

#         serializer = serializersUser(users, many=True)
#         return JsonResponse(serializer.data, safe=False)


# @api_view(['GET'])
# def get_user(request, id):
#     if request.method == 'GET':
#         user = Usuarios.objects.get(pk=id)
#         user_data = {
#             'name': user.name,
#             'dni': user.dni,
#             'password': user.password,
#             'is_admin': user.is_admin,
#             'id': user.id,
#         }
#         serializer = serializersUser(user, data=user_data)
#         if serializer.is_valid():
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @swagger_auto_schema(methods=['post'], request_body=serializersUser)
# @api_view(['POST'])
# def check_user(request):
#     if request.method == 'POST':
#         try:
#             user = Usuarios.objects.get(email=request.data['email'])
#             # print(user.user_name)
#             # print(user.user_password)
#             # print(request.data['user_password'])
#             # print(user.is_admin)
#             # print(request.data['is_admin'])
#             user_data = {
#                 'name': user.name,
#                 'user_password': user.password,
#                 'dni': user.dni,
#                 "email": user.email,
#                 "cellPhone": user.cellPhone,
#             }
#             # print(user_data)
#             serializer = serializersUser(user, data=user_data)
#             if user:
#                 match_password = check_password(
#                     request.data['user_password'], user.password)
#                 print(match_password)
#                 if match_password and user.is_admin == request.data['is_admin']:
#                     print(serializer.is_valid())
#                     if serializer.is_valid():
#                         return JsonResponse(serializer.data, safe=False)
#                     else:
#                         return JsonResponse("You are not authorized to access dashboad", safe=False)
#                 else:
#                     return JsonResponse("Email or password incorrect", safe=False)
#             else:
#                 return JsonResponse("Email or password incorrect", safe=False)
#         except:
#             return JsonResponse("Account doesn't exist", safe=False)


# @api_view(['PUT'])
# def change_password(request):
#     if request.method == 'PUT':
#         user = Usuarios.objects.get(name=request.data['name'])
#         print(user.name)
#         print(user.password)
#         print(request.data['password'])
#         print(request.data['old_password'])
#         if user:
#             match_password = check_password(
#                 request.data['old_password'], user.password)
#             pass_encrypt = make_password(request.data['password'])
#             print(match_password)
#             if match_password:
#                 new_data = {
#                     'name': user.name,
#                     'password': pass_encrypt,
#                     'is_admin': user.is_admin,
#                     'id': user.id,
#                     "email": user.email,
#                     "cellPhone": user.cellPhone,
                    
#                 }
#                 serializer = serializersUser(user, data=new_data)
#                 if serializer.is_valid():
#                     serializer.save()
#                     return JsonResponse(serializer.data)
#                 return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#             else:
#                 return JsonResponse("Email or password incorrect", safe=False)
#         else:
#             return JsonResponse("Email or password incorrect", safe=False)


