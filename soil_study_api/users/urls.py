from django.urls import path

from .views import login, get_user_information

urlpatterns = [
    path('login', login),
    path('info', get_user_information),
    #path('current_user/', current_user),
   # path('users/', UserList.as_view())
    # path('login-post-crud',views.registerCRUD),
    # path('login-get',views.register_get_CRUD),
    # path('details',views.register_details),
    # path('users-get', views.user_get)
    # path('all_user/', views.all_user),
    # path('add_user/', views.add_user),
    # path('get_user_edit/<int:id>', views.get_user),
    # path('login-user/', views.check_user),
    # path('change_password/', views.change_password),
    #path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    
    
]