# syntax=docker/dockerfile:1
FROM python:3.7
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
RUN chmod 766 -R . && chmod 755 -R ./data
RUN useradd dev -ms /bin/sh && chown -R dev /code && chown -R dev /home
USER dev
